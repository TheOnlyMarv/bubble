﻿using System;

namespace Bubble
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                if (args[0].ToLower() == "-c")
                {
                    BubbleBuilder.getInstance(args).start();
                    return;
                }
                else
                {
                    BubbleManipulator.getInstance(args).start();
                    return;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("An der Stelle, an der eine Zahl erwartet wurde, sind Zeichen!");
            }
            catch (Exception)
            {
                Console.WriteLine("Manipulieren von bildern:");
                Console.WriteLine("[SourcePath] [DestinationPath] [Modifier]");
                Console.WriteLine("Modifier: Eine Zahl > 0, welche die Größe angibt\n");
                Console.WriteLine("Erzeugen eines zufälligen Bubblebilds:");
                Console.WriteLine("-c [DestinationPath] [Bubble X-Richtung] [Bubble X-Richtung] [Bubblesize]");
                return;
            }
        }
    }
 }

