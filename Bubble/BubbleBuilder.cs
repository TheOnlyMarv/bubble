﻿using System;
using System.Drawing;
using System.Collections.Generic;
public class BubbleBuilder
{
    private static BubbleBuilder instance = null;
    private String savePath = null;

    private int sizeBubbleMax = 20;
    private int countBubbleX = 0;
    private int countBubbleY = 0;
    private Random rnd = new Random();

    private BubbleBuilder(string[] args)
	{
        parseArgs(args);
	}

    public static BubbleBuilder getInstance(string[] args)
    {
        if (instance == null)
            instance = new BubbleBuilder(args);
        return instance;
    }

    private void parseArgs(string[] args)
    {
        this.savePath = args[1];
        try
        {
            countBubbleX = Convert.ToInt32(args[2]);
            countBubbleY = Convert.ToInt32(args[3]);
            sizeBubbleMax = Convert.ToInt32(args[4]);
        }
        catch (FormatException)
        {
            throw new FormatException();
        }
    }

    public void start()
    {
        Bitmap generatedImage = new Bitmap(countBubbleX * sizeBubbleMax, countBubbleY * sizeBubbleMax);
        List<Cycle> cyclesWithMaxBubbleSize = generateCyclesWithMaxBubbleSize();
        cyclesWithMaxBubbleSize = pickRandomBubbles(cyclesWithMaxBubbleSize);
        drawCycleFromList(cyclesWithMaxBubbleSize, generatedImage);
        saveImage(generatedImage);
    }

    private List<Cycle> pickRandomBubbles(List<Cycle> cycleList)
    {
        int countRandom = (int) (countBubbleX * countBubbleY * 10);
        for (int i = 0; i < countRandom; i++)
            cycleList.AddRange(cycleList[getRandomNumber(0,cycleList.Count-1)].splitCycle());
        return cycleList;
    }

    private void drawCycleFromList(List<Cycle> cycleList, Bitmap generatedImage)
    {
        Color rndColor;
        Graphics gr = Graphics.FromImage(generatedImage);
        gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        foreach (Cycle cycle in cycleList)
        {
            rndColor = getRandomColor();
            gr.FillEllipse(new SolidBrush(rndColor), cycle.getRectangle());
        }
    }

    private List<Cycle> generateCyclesWithMaxBubbleSize()
    {
        List<Cycle> returnValue = new List<Cycle>();
        int counterX = 0;
        int counterY = 0;
        while (counterY != countBubbleY)
        {
            while (counterX != countBubbleX)
            {
                returnValue.Add(new Cycle(new Rectangle(counterX * sizeBubbleMax, counterY * sizeBubbleMax, sizeBubbleMax, sizeBubbleMax)));
                counterX += 1;
            }
            counterX = 0;
            counterY += 1;
        }
        return returnValue;
    }

    private Color getRandomColor()
    {
        return Color.FromArgb(getRandomNumber(0, 255), getRandomNumber(0, 255), getRandomNumber(0, 255));
    }

    private int getRandomNumber(int min, int max)
    {
        return rnd.Next(min, max);
    }

    private void saveImage(Bitmap generatedImage)
    {
        try { 
            generatedImage.Save(this.savePath);
        }
        catch (Exception)
        {
            Console.WriteLine("Ungültiges Ziel");
        }        
    }
}

public class Cycle
{
    private Rectangle rectangle;

    public Cycle(Rectangle rectangle)
    {
        this.rectangle = rectangle;
    }

    public Rectangle getRectangle()
    {
        return rectangle;
    }

    public Cycle[] splitCycle()
    {
        Cycle[] returnValue = new Cycle[3];
        rectangle = new Rectangle(rectangle.X, rectangle.Y, rectangle.Width / 2, rectangle.Height / 2);
        returnValue[0] = new Cycle(new Rectangle(rectangle.X + rectangle.Width, rectangle.Y, rectangle.Width, rectangle.Height));
        returnValue[1] = new Cycle(new Rectangle(rectangle.X, rectangle.Y + rectangle.Height, rectangle.Width, rectangle.Height));
        returnValue[2] = new Cycle(new Rectangle(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height, rectangle.Width, rectangle.Height));
        return returnValue;
    }
}
