﻿using System;
using System.Drawing;

namespace Bubble
{
    class BubbleManipulator
    {
        private static BubbleManipulator instance;
        private String originalImagePath;
        private String newImagePath;
        private int modifier;

        Bitmap originalImage;
        Bitmap newImage;

        private BubbleManipulator(String[] args)
        {
            parseArgs(args);
        }

        public static BubbleManipulator getInstance(String[] args)
        {
            if (instance == null)
                instance = new BubbleManipulator(args);
            return instance;
        }

        private void parseArgs(String[] args)
        {
            originalImagePath = args[0];
            newImagePath = args[1];
            try
            {
                modifier = Convert.ToInt32(args[2]);
            }
            catch (FormatException)
            {
                throw new FormatException();
            }
        }

        public void start()
        {
            try
            {
                loadImageFromFile();
                modifyImage();
                safeImageIntoFile();
            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                Console.WriteLine(fnfe.ToString());
            }
            catch (System.IO.DirectoryNotFoundException dnfe)
            {
                Console.WriteLine(dnfe.ToString());
            }
        }

        private void loadImageFromFile()
        {
            try
            {
                originalImage =  new Bitmap(Bitmap.FromFile(originalImagePath));
            }
            catch (System.IO.FileNotFoundException)
            {
                throw new System.IO.FileNotFoundException(originalImagePath + " not found");
            }
        }
        private void modifyImage()
        {
            newImage = new Bitmap(originalImage.Size.Width, originalImage.Size.Height);
            int startX = 0;
            int startY = 0;
            if (modifier > Math.Min(originalImage.Size.Width, originalImage.Size.Height))
                modifier = Math.Min(originalImage.Size.Width, originalImage.Size.Height);
            Graphics gr = Graphics.FromImage(newImage);
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            while (startY <= originalImage.Size.Height - modifier)
            {
                while (startX <= originalImage.Size.Width - modifier)
                {
                    Color middleColor = calcMiddleColor(originalImage, startX, startY, startX + modifier, startY + modifier);
                    SolidBrush pen = new SolidBrush(middleColor);
                    gr.FillEllipse(pen, startX, startY, modifier, modifier);

                    startX += modifier;
                }
                startX = 0;
                startY += modifier;
            }
        }

        private Color calcMiddleColor(Bitmap image, int startX, int startY, int endX, int endY)
        {
            OwnColor[] colors = new OwnColor[endX - startX];
            int counter = 0;
            while (startX < endX)
            {
                while (startY < endY)
                {
                    try
                    {
                        colors[counter++] = new OwnColor(image.GetPixel(startX, startY));
                    }
                    catch (System.ArgumentOutOfRangeException)
                    { }

                    startY++;
                }
                startX++;
            }
            return OwnColor.calcMiddleColor(colors);
        }

        private void safeImageIntoFile()
        {
            try
            {
                newImage.Save(newImagePath);
            }
            catch (Exception)
            {
                throw new System.IO.DirectoryNotFoundException(newImagePath + " destination not found.");
            }
        }
    }

    public class OwnColor
    {
        int a;
        int r;
        int g;
        int b;
        public OwnColor(Color color)
        {
            this.a = color.A;
            this.r = color.R;
            this.g = color.G;
            this.b = color.B;
        }

        public Color getColor()
        {
            return Color.FromArgb(a, r, g, b);
        }

        public static Color calcMiddleColor(OwnColor[] ownColors)
        {
            int a = 0, r = 0, g = 0, b = 0;
            foreach (OwnColor ownColor in ownColors)
            {
                a += ownColor.a;
                r += ownColor.r;
                g += ownColor.g;
                b += ownColor.b;
            }
            a /= ownColors.Length;
            r /= ownColors.Length;
            g /= ownColors.Length;
            b /= ownColors.Length;
            return Color.FromArgb(a, r, g, b);
        }
    }
}
